package br.edu.ifsc.loja.controller;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import br.edu.ifsc.loja.model.Usuario;

public class UsuarioControllerTest {

	@Test
	public void listaTest() {
		List<Usuario> lista = UsuarioController.getInstance().lista();
		assertEquals(1, lista.size());
	}

}
