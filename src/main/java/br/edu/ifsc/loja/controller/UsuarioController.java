package br.edu.ifsc.loja.controller;

import java.util.List;

import br.edu.ifsc.loja.dao.UsuarioDAO;
import br.edu.ifsc.loja.model.Usuario;

public class UsuarioController {

	private static UsuarioController instance;

	public static synchronized UsuarioController getInstance(){
	
		if (instance == null){
			instance = new UsuarioController();
		}
		return instance;
	}
	
	private UsuarioController(){
	}

	public void cadastrar(Usuario usuario) {
		if (usuario != null){
			UsuarioDAO.getInstance().insert(usuario);
		}
	}

	public List<Usuario> lista(){
		return UsuarioDAO.getInstance().all();
	}
}
