package br.edu.ifsc.loja.controller;

import java.math.BigDecimal;

import br.edu.ifsc.loja.dao.CarrinhoDAO;
import br.edu.ifsc.loja.dao.ItemCompraDAO;
import br.edu.ifsc.loja.model.Carrinho;
import br.edu.ifsc.loja.model.ItemCompra;
import br.edu.ifsc.loja.model.Produto;
import br.edu.ifsc.loja.model.Usuario;

public class CarrinhoController {

	private static CarrinhoController instance;

	public static synchronized CarrinhoController getInstance() {

		if (instance == null) {
			instance = new CarrinhoController();
		}
		return instance;
	}

	public void adiciona(Carrinho carrinho, Produto produto) {

		ItemCompra itemCompra = carrinho.findItemByProduto(produto.getId());

		if (itemCompra != null) {
			carrinho.updateItemCompra(itemCompra, 1);
			ItemCompraDAO.getInstance().update(itemCompra);
		} else {
			itemCompra = new ItemCompra(produto, new Integer(1));
			carrinho.addItemCompra(itemCompra);
			ItemCompraDAO.getInstance().insert(itemCompra);
		}
		
		CarrinhoDAO.getInstance().update(carrinho);
	}

	public Carrinho getCarrinhoUsuario(Usuario usuario) {
		return CarrinhoDAO.getInstance().getCarrinhoUsuario(usuario);
	}

	public void updateTotal(Carrinho carrinho) {
		BigDecimal total = new BigDecimal(0);

		for (ItemCompra itemCompra : carrinho.getListItemCompra()) {
			total = total.add(itemCompra.getProduto().getValor().multiply(new BigDecimal(itemCompra.getQuantidade())));
		}

		carrinho.setValor(total);

		CarrinhoDAO.getInstance().update(carrinho);
	}

	public void removeItem(Carrinho carrinho, Integer idProduto) {
		ItemCompra itemCompra = carrinho.removeItemCompra(idProduto);
		ItemCompraDAO.getInstance().delete(itemCompra);
		CarrinhoDAO.getInstance().update(carrinho);
	}

	public void updateItemCompra(ItemCompra itemCompra) {
		ItemCompraDAO.getInstance().update(itemCompra);

		updateTotal(itemCompra.getCarrinho());

	}
}
