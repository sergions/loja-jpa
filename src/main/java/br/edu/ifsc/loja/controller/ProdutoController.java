package br.edu.ifsc.loja.controller;

import java.util.List;

import br.edu.ifsc.loja.dao.ProdutoDAO;
import br.edu.ifsc.loja.model.Produto;

public class ProdutoController {
	private static ProdutoController instance;

	public static synchronized ProdutoController getInstance(){
	
		if (instance == null){
			instance = new ProdutoController();
		}
		return instance;
	}

	public List<Produto> lista(){
		return ProdutoDAO.getInstance().all();
	}
}
