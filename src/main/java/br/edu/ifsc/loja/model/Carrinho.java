package br.edu.ifsc.loja.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


/**
 * The persistent class for the carrinho database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Carrinho.listAll", query="SELECT c FROM Carrinho c"),
	@NamedQuery(name="Carrinho.findByUsuarioId", query="SELECT c FROM Carrinho c WHERE c.usuario.id = :idUsuario"),
})
public class Carrinho extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private BigDecimal valor;

	@OneToOne
	@JoinColumn(name = "usuario_id", referencedColumnName = "id")
	private Usuario usuario;

	@OneToMany(mappedBy="carrinho")
	private List<ItemCompra> listItemCompra;

	public Carrinho() {
			this(new Usuario());
	}
	
	public Carrinho(Usuario usuario){
		this.setUsuario(usuario);
		this.valor = new BigDecimal(0);
		this.listItemCompra = new ArrayList<ItemCompra>();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getValor() {
		return this.valor;
	}


	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<ItemCompra> getListItemCompra() {
		return this.listItemCompra;
	}


	public ItemCompra addItemCompra(ItemCompra itemCompra) {
		
		itemCompra.setCarrinho(this);
		this.listItemCompra.add(itemCompra);
		
		this.valor = this.valor.add(itemCompra.getProduto().getValor().multiply(new BigDecimal(itemCompra.getQuantidade())));
		
		return itemCompra;
	}

	public ItemCompra removeItemCompra(ItemCompra itemCompra) {
		
		this.valor = this.valor.subtract(itemCompra.getProduto().getValor().multiply(new BigDecimal(itemCompra.getQuantidade())));
		
		this.listItemCompra.remove(itemCompra);

		return itemCompra;
	}
	
	public ItemCompra removeItemCompra(Integer idProduto) {
		ItemCompra itemCompra = this.findItemByProduto(idProduto);
		return this.removeItemCompra(itemCompra);
	}
	
	public void updateItemCompra(ItemCompra itemCompra, Integer quantidade) {
		
		itemCompra.setQuantidade(itemCompra.getQuantidade() + quantidade);
		
		this.valor = this.valor.add(itemCompra.getProduto().getValor().multiply(new BigDecimal(quantidade)));
	}

	public ItemCompra findItemByProduto(Integer produtoId){
		ItemCompra itemCompra = null;
		Iterator<ItemCompra> it = this.getListItemCompra().iterator();
		while (it != null && it.hasNext()){
			itemCompra = it.next();
			if (itemCompra.getProduto().getId().equals(produtoId)){
				return itemCompra;
			}
		}
		return null;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

}