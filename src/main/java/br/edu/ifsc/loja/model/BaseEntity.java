package br.edu.ifsc.loja.model;

public abstract class BaseEntity{

	public abstract Integer getId();

	public abstract void setId(Integer id);
	
}
