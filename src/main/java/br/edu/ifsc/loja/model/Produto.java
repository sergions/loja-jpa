package br.edu.ifsc.loja.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;

/**
 * The persistent class for the produto database table.
 * 
 */
@Entity
@NamedQueries({ 
	@NamedQuery(name = "Produto.listAll", query = "SELECT p FROM Produto p"), 
	@NamedQuery(name = "Produto.findById", query = "SELECT p FROM Produto p WHERE p.id = :id"), 
})
public class Produto extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private Integer codigo;

	private String descricao;

	private String imagem;

	private String nome;

	private BigDecimal valor;

	public Produto() {
		this(new Integer(0), "", "", new BigDecimal(0.0), "");
	}

	public Produto(Integer codigo, String nome, String descricao, BigDecimal valor, String imagem) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.descricao = descricao;
		this.valor = valor;
		this.imagem = imagem;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCodigo() {
		return this.codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getImagem() {
		return this.imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public BigDecimal getValor() {
		return this.valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

}