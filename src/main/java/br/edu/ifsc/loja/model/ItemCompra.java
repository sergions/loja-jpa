package br.edu.ifsc.loja.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the item_compra database table.
 * 
 */
@Entity
@Table(name="item_compra")
@NamedQuery(name="ItemCompra.findAll", query="SELECT i FROM ItemCompra i")
public class ItemCompra extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private BigDecimal valor;

	@ManyToOne
	@JoinColumn(name="carrinho_id", referencedColumnName="id")
	private Carrinho carrinho;

	@ManyToOne
	@JoinColumn(name="produto_id", referencedColumnName="id")
	private Produto produto;
	
	private Integer quantidade;

	public ItemCompra() {
		this(new Produto(), 0);
	}

	public ItemCompra(Produto produto, Integer quantidade) {
		this.produto = produto;
		this.quantidade = quantidade;
		this.valor = produto.getValor().multiply(new BigDecimal(quantidade));
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getValor() {
		return this.valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Carrinho getCarrinho() {
		return this.carrinho;
	}

	public void setCarrinho(Carrinho carrinho) {
		this.carrinho = carrinho;
	}

	public Produto getProduto() {
		return this.produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

}