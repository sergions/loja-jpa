/*
 * @author  Sérgio Nicolau da Silva <sergions@gmail.com>
 * Adaptado do DAO.java do exercício Livraria
 */
package br.edu.ifsc.loja.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import br.edu.ifsc.loja.model.BaseEntity;

public abstract class GenericDAO<M extends BaseEntity> implements InterfaceDAO<M> {
	
	private static EntityManager em;

	private Class<M> classe;
	
	public GenericDAO(Class<M> classe) {
		GenericDAO.em  = Persistence.createEntityManagerFactory("loja-jpa").createEntityManager();
		this.classe = classe;
	}
	
	protected EntityManager getEntityManager(){
		return GenericDAO.em;
	}
	
	@Override
	public boolean insert(M entity) {
		em.getTransaction().begin();
		em.persist(entity);
		em.getTransaction().commit();
		return (entity.getId() != null);
	}

	@Override
	public boolean delete(M entity) {
		boolean sucesso = true;
		try {
			em.remove(entity);
		} catch (NoResultException e) {
			sucesso = false;
		}		
		return sucesso;
	}
	
	@Override
	public M findById(Integer id) {
		String namedQueryById = classe.getSimpleName() + ".findById";
		Query query = em.createNamedQuery(namedQueryById, classe);
		query.setParameter("id", id);
		try {
			return (M) query.getSingleResult();	
		} catch (NoResultException e) {
			return null;
		}
		
	}
	
	@Override
	public List<M> all() {
		String namedQueryById = classe.getSimpleName() + ".listAll";
		Query query = em.createNamedQuery(namedQueryById, classe);
		try {
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	
	@Override
	public boolean update(M entity) {
		em.getTransaction().begin();
		em.merge(entity);
		em.getTransaction().commit();
		return (entity.getId() != null);
	}

}
