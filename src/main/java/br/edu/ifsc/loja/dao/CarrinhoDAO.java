package br.edu.ifsc.loja.dao;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.edu.ifsc.loja.model.Carrinho;
import br.edu.ifsc.loja.model.Usuario;

public class CarrinhoDAO extends GenericDAO<Carrinho> {
	private static CarrinhoDAO instancia;

	public static synchronized CarrinhoDAO getInstance() {
		if (instancia == null) {
			instancia = new CarrinhoDAO();
		}
		return instancia;
	}

	public CarrinhoDAO() {
		super(Carrinho.class);
	}

	public Carrinho getCarrinhoUsuario(Usuario usuario) {
		Carrinho carrinho = null;

		Query query = this.getEntityManager().createNamedQuery("Carrinho.findByUsuarioId");
		query.setParameter("idUsuario", usuario.getId());
		try {
			carrinho = (Carrinho) query.getSingleResult();
		} catch (NoResultException e) {
			carrinho = null;
		}

		if (carrinho == null) {
			carrinho = new Carrinho(usuario);
			super.insert(carrinho);
		}
		return carrinho;

	}
}
