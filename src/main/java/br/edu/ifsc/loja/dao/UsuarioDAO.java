package br.edu.ifsc.loja.dao;

import java.util.Iterator;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.edu.ifsc.loja.model.Carrinho;
import br.edu.ifsc.loja.model.Usuario;

public class UsuarioDAO extends GenericDAO<Usuario> {
	private static UsuarioDAO instancia;

	public static synchronized UsuarioDAO getInstance() {
		if (instancia == null) {
			instancia = new UsuarioDAO();
		}
		return instancia;
	}

	public UsuarioDAO() {
		super(Usuario.class);
		this.populateUsuario();
	}

	public Usuario findByEmailESenha(String login, String senha) {
		
		Usuario usuario = null;

		Query query = this.getEntityManager().createNamedQuery("Usuario.findByLoginAndSenha");
		query.setParameter("login", login);
		query.setParameter("senha", senha);
		try {
			usuario = (Usuario) query.getSingleResult();
		} catch (NoResultException e) {
			usuario = null;
		}
		return usuario;
	}
	
	private void populateUsuario(){

		Usuario usuario = findByEmailESenha("sergio", "123");
		if (usuario == null){
			usuario = new Usuario();
			usuario.setNome("Sérgio Nicolau da Silva");
			usuario.setCpf("871.547.309-00");
			usuario.setLogin("sergio");
			usuario.setSenha("123");
			this.insert(usuario);
		}
		
	}

}