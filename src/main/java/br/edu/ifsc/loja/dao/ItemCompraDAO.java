package br.edu.ifsc.loja.dao;

import br.edu.ifsc.loja.model.ItemCompra;

public class ItemCompraDAO extends GenericDAO<ItemCompra> {

	private static ItemCompraDAO instancia;

	public static synchronized ItemCompraDAO getInstance() {
		if (instancia == null) {
			instancia = new ItemCompraDAO();
		}
		return instancia;
	}

	public ItemCompraDAO() {
		super(ItemCompra.class);
	}
}
