package br.edu.ifsc.loja.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

import br.edu.ifsc.loja.controller.CarrinhoController;
import br.edu.ifsc.loja.controller.ProdutoController;
import br.edu.ifsc.loja.model.Carrinho;
import br.edu.ifsc.loja.model.ItemCompra;
import br.edu.ifsc.loja.model.Produto;
import br.edu.ifsc.loja.model.Usuario;

@ManagedBean
@ViewScoped
public class LojaBean implements Serializable {

	private static final long serialVersionUID = 7075754873356864341L;

	private List<Produto> produtos;

	private Carrinho carrinho;

	private Usuario usuario;

	public void setCarrinho(Carrinho carrinho) {
		this.carrinho = carrinho;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Carrinho getCarrinho() {
		FacesContext context = FacesContext.getCurrentInstance();

		if (this.carrinho == null) {

			this.usuario = (Usuario) context.getExternalContext().getSessionMap().get("usuarioLogado");

			this.carrinho = CarrinhoController.getInstance().getCarrinhoUsuario(this.usuario);
		}
		return carrinho;
	}

	public List<Produto> getProdutos() {
		if (this.produtos == null) {
			this.produtos = ProdutoController.getInstance().lista();
		}
		return produtos;
	}

	public void update(Integer produtoId){
		ItemCompra itemCompra = carrinho.findItemByProduto(produtoId);
		CarrinhoController.getInstance().updateItemCompra(itemCompra);
	}
	
	public void add(Produto produto) {
		if (this.carrinho == null) {
			getCarrinho();
		}
		CarrinhoController.getInstance().adiciona(carrinho, produto);
	}

	public void remove(Integer produtoId) {
		System.out.println("REMOVER: " + produtoId);
		CarrinhoController.getInstance().removeItem(carrinho, produtoId);
	}

}
